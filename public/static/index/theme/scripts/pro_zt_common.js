/*产品专题页JS
*******************************/
			//导航下拉菜单

			$(function() {

				teamtopap(".MainNav li");

			});

			function teamtopap(_this) {

				$(_this).each(function() {

					var $this = $(this);

					var theMenu = $this.find(".box");

					var tarHeight = theMenu.height();

					theMenu.css({ height: 0 });

					$this.hover(

						function() {

							$(this).addClass("li_hover");

							theMenu.stop().show().animate({ height: tarHeight }, 400);

						},

						function() {

							$(this).removeClass("li_hover");

							theMenu.stop().animate({ height: 0 }, 400, function() {

								$(this).css({ display: "none" });

							});

						}

					);

				});

			}

			//头部微信显示
   $(function(){
   	  $(".TopWx").bind("mouseover", function() {

				jQuery(".WxUpShow").slideDown();

			})

			$(".TopWx").bind("mouseleave", function() {

				jQuery(".WxUpShow").slideUp();

			})
   })
			
			
			
//商务通

				$(function() {

					var i = 1;

					var swt_sb = $('#proZtSwt_sidebar');

					var swt_ct = $('#proZtSwt_center');

					var aa = 760;

					$(window).scroll(function() {

						var sc = $(window).scrollTop();

						if(sc > aa && i == 1) {

							i = 0;

							swt_ct.show();

						} else if(sc < 100 && i == 0)

						{ i = 1; }

					});

					$(".proZtSwt_center_close").click(function() {

						swt_ct.hide();

						return false;

					});

				});			
//右边弹窗
$(function() {

					$('.swt_h').prev().addClass('swt_current_t');

					$('.swt_tit').each(function() {

						$(this).click(function() {

							$(this).toggleClass('swt_current_t').next().slideToggle()

						})

					});

					$('.newswt_close').click(function() {

						$('#newswt').animate({

							width: '-=130'

						}, 400, function() {

							$(this).hide()

							$('#newswt_mini').animate({

								width: '+=40'

							}, 600)

						})

					});

					$('#newswt_mini').click(function() {

						$(this).animate({

							width: '-=40'

						}, 100, function() {

							$('#newswt').show().animate({

								width: '+=130'

							}, 400)

						})

					})

				})




			
//标签切换
$(function(){
	var h = $("#pro_zt_tab div:first").height(); 
	var cont = $("#pro_zt_tab div:first > ul");
	var btn = $("#pro_zt_tab > ul a");
	var index;
	btn.mouseover(function() {
		index = btn.index(this);
		btn.removeClass("active");
		$(this).addClass("active");
		showTab(index);
	});
	function showTab(index){
		var rollTop = -index*h;
		cont.stop(true,false).animate({"top":rollTop});
		
	};
});
//标签切换
$(function(){
	var h2 = $("#pro_zt_tab2 div:first").height(); 
	var cont2 = $("#pro_zt_tab2 div:first > ul");
	var btn2 = $("#pro_zt_tab2 > ul a");
	var index;
	btn2.mouseover(function() {
		index = btn2.index(this);
		btn2.removeClass("active");
		$(this).addClass("active");
		showTab(index);
	});
	function showTab(index){
		var rollTop = -index*h2;
		cont2.stop(true,false).animate({"top":rollTop});
		
	};
});
//案例切换
$(function(){
	var h = $("#pro_zt_caseMask li").height();
	var picNumber = $("#pro_zt_caseMask li").length;  //获取图片张数
	var index = 0;
	var picTimer;
	//上一页按钮
	$("#pro_zt_caseMask + .prev").click(function() {
		index -= 1;
		if(index == -1) {index = picNumber - 1;}
		showPics(index);
	});
	//下一页按钮
	$("#pro_zt_caseMask + .prev + .next").click(function() {
		index += 1;
		if(index == picNumber) {index = 0;}
		showPics(index);
	});
	$("#pro_zt_caseMask").parent().hover(function() {  //鼠标划入停止，划出继续动画
		clearInterval(picTimer);
	},function() {
		picTimer = setInterval(function() {
			showPics(index);
			index++;
			if(index == picNumber) {index = 0;}
		},5000); 
	}).trigger("mouseleave");
	function showPics(index){  //图片滚动函数
		var rollTop = -index*h;
		$("#pro_zt_caseMask > ul").stop(true,false).animate({"top":rollTop},300);
	};
});
/*表单验证*/
$(function(){
	$(".verify").bind("submit",function(){
		//姓名
		var reg_name = /^[\u4e00-\u9fa5]+$/;
		var field_name = $(this).find('.verify_name').attr('value'); 
		var v_name = $(this).find('.verify_name')
		if(v_name.length>0 & !reg_name.test(field_name)){
			alert('请输入正确的中文名字');
			return false;
		}
		//邮箱
		var reg_email = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var field_email = $(this).find('.verify_email').val(); 
		var v_email = $(this).find('.verify_email')
		if(v_email.length>0 & !reg_email.test(field_email)){
			alert('请输入正确的邮箱地址');
			return false;
		}
		//网址
		var reg_website = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
		var field_website = $(this).find('.verify_website').val(); 
		var v_website = $(this).find('.verify_website')
		if(v_website.length>0 & !reg_website.test(field_website)){
			alert('请输入正确的网址');
			return false;
		}
		//电话
		var reg_tel = /((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/;
		var field_tel = $(this).find('.verify_tel').val(); 
		var v_tel = $(this).find('.verify_tel')
		if(v_tel.length>0 & !reg_tel.test(field_tel)){
			alert('请输入正确的电话或手机号码');
			return false;
		}
		//非空
		var isunnull = true;
		$(this).find('.verify_unnull').each(function(){
			if($(this).val()==''){
				alert('请输入'+$(this).attr('data-name'));
				isunnull = false;
				return false;
			}
		});
		return isunnull;
	})
})
/*返回顶部*/
$(function(){
	if($('#swt_message').length > 0){
		$("<a id='backToTop_swt' href='javascript:void(0)' title='返回顶部'>返回顶部</a>").appendTo("#swt_message,#swt_message_mini");
		$("#backToTop_swt").hide();
	}
	else{
		$("<a id='backToTop' href='avascript:void(0)' title='返回顶部'>返回顶部</a>").appendTo("body")
		$("#backToTop").hide();
	}
    $('#swt_message_mini,#swt_message').click(function(){
		if ($(window).scrollTop()>100){
			$("#backToTop,#backToTop_swt").show();
		}else{
 			$("#backToTop,#backToTop_swt").hide();
		}
	})
	$(window).scroll(function(){
		if ($(window).scrollTop()>100){
			$("#backToTop,#backToTop_swt").fadeIn(400);
		}else{
 			$("#backToTop,#backToTop_swt").fadeOut(600);
		}
	});
	$("#backToTop,#backToTop_swt").click(function(){
		$('body,html').animate({scrollTop:0},300);
		return false;
	})
});
//验证码
$(function(){
        //点击变更验证码
        $('.vimg').click(function(){
			var src = $(this).attr('src');
            $(this).attr('src', src + '?rand=' + Math.random()*(200-50));
        });
        $('.cimg').click(function(){
			var src2 = $(this).siblings('.vimg').attr('src');
            $(this).siblings('.vimg').attr('src', src2 + '?rand=' + Math.random()*(200-50));
        });
		//自动大写
		$('.vcode').keyup(function(){
			$(this).attr('value',$(this).attr('value').toUpperCase());			  
		});
		//刷新图片
		$('.vimg').trigger("click")
})
/*商务通链接加当前页url*/
$(function(){
	$('a[href^="http://chat.teamtop.com:50003/LR/Chatpre.aspx"]').each(function(){this.href = this.href + '&p=' + escape(location.href)});
})