<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:85:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\gongs\gongs_list.html";i:1534932260;s:82:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\public\header.html";i:1535017565;s:83:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        博胜天达
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
              <a><cite>关于我们</cite></a>
              <a><cite>公司介绍</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right"  href="javascript:location.replace(location.href);" title="刷新"><i class="layui-icon" style="line-height:30px">ဂ</i></a>
        </div>
        <div class="x-body">
            <xblock>

                <button class="layui-btn" onclick="banner_add('添加','<?php echo url("create"); ?>','600','500')">
                    <i class="layui-icon">&#xe608;</i>
                    添加
                </button>
                <span class="x-right" style="line-height:40px">共有数据：<?php echo $count; ?> 条</span>
            </xblock>
            <table class="layui-table">
                <thead>
                    <tr>

                        <th>
                            ID
                        </th>
                        <th>
                            缩略图
                        </th>
                        <th>
                            简介
                        </th>

                        <th>
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody id="x-img">

                <?php if(is_array($banner) || $banner instanceof \think\Collection || $banner instanceof \think\Paginator): $i = 0; $__LIST__ = $banner;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <tr>

                        <td>
                            <?php echo $vo['id']; ?>
                        </td>
                        <td>
                            <img  src="__ROOT__/uploads/<?php echo $vo['image']; ?>" width="150" alt="">点击图片试试
                        </td>
                        <td >
                            <?php echo $vo['des']; ?>
                        </td>

                        <td class="td-manage">

                            <a title="编辑" href="javascript:;" onclick="banner_edit('编辑','<?php echo url("edit"); ?>'+'?id='+<?php echo $vo['id']; ?>,'4','','510')"
                            class="ml-5" style="text-decoration:none">
                                <i class="layui-icon">&#xe642;</i>
                            </a>
                            <a title="删除" href="javascript:;" onclick="banner_del(this,'<?php echo $vo['id']; ?>')"
                            style="text-decoration:none">
                                <i class="layui-icon">&#xe640;</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
            </table>

            <div id="page" style="text-align: center"><?php echo $banner->render(); ?></div>
        </div>
        <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>
        <script>
            layui.use(['laydate','element','laypage','layer'], function(){
                $ = layui.jquery;//jquery
              laydate = layui.laydate;//日期插件
              lement = layui.element();//面包导航
              laypage = layui.laypage;//分页
              layer = layui.layer;//弹出层

              //以上模块根据需要引入

                layer.ready(function(){ //为了layer.ext.js加载完毕再执行
                  layer.photos({
                    photos: '#x-img'
                    //,shift: 5 //0-6的选择，指定弹出图片动画类型，默认随机
                  });
                }); 
              
            });

            //批量删除提交
             function delAll () {
                layer.confirm('确认要删除吗？',function(index){
                    //捉到所有被选中的，发异步进行删除
                    layer.msg('删除成功', {icon: 1});
                });
             }
             /*添加*/
            function banner_add(title,url,w,h){
                x_admin_show(title,url,w,h);
            }
             /*停用*/
            function banner_stop(obj,id){
                layer.confirm('确认不显示吗？',function(index){
                    //发异步把用户状态进行更改
                    $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="banner_start(this,id)" href="javascript:;" title="显示"><i class="layui-icon">&#xe62f;</i></a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="layui-btn layui-btn-disabled layui-btn-mini">不显示</span>');
                    $(obj).remove();
                    layer.msg('不显示!',{icon: 5,time:1000});
                });
            }

            /*启用*/
            function banner_start(obj,id){
                layer.confirm('确认要显示吗？',function(index){
                    //发异步把用户状态进行更改
                    $(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="banner_stop(this,id)" href="javascript:;" title="不显示"><i class="layui-icon">&#xe601;</i></a>');
                    $(obj).parents("tr").find(".td-status").html('<span class="layui-btn layui-btn-normal layui-btn-mini">已显示</span>');
                    $(obj).remove();
                    layer.msg('已显示!',{icon: 6,time:1000});
                });
            }
            // 编辑
            function banner_edit (title,url,id,w,h) {
                x_admin_show(title,url,w,h); 
            }
            /*删除*/
            function banner_del(obj,id){
                layer.confirm('确认要删除吗？',function(index){
                    //发异步删除数据
                    $.get("<?php echo url('delete'); ?>",{id:id});


                    $(obj).parents("tr").remove();
                    layer.msg('已删除!',{icon:1,time:1000});
                });
            }
            </script>

    </body>
</html>