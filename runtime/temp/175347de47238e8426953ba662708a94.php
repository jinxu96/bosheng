<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:95:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\categoryxx\categoryxx_edit.html";i:1533203744;s:82:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\public\header.html";i:1535017565;s:83:"D:\PHPTutorial\WWW\pen\bosheng\public/../application/admin\view\public\base_js.html";i:1533819104;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>
        博胜天达
    </title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="__STATIC__/admin/css/x-admin.css" media="all">
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript" src="__ROOT__/ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
          UE.getEditor('content',{    //content为要编辑的textarea的id
          initialFrameWidth: 1100,   //初始化宽度
          initialFrameHeight: 500,   //初始化高度
  });
</script>
</head>
    
    <body>
        <div class="x-body">
            <form class="layui-form">
                <div class="layui-form-item">
                    <label for="id" class="layui-form-label">
                        ID
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="id" name="pid" required="" lay-verify="required"
                        autocomplete="off"  value="<?php echo $cate_now['pid']; ?>" disabled="" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label for="cate_name" class="layui-form-label">
                        <span class="x-red">*</span>分类名
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="cate_name" name="cate_name" required="" lay-verify="required"
                        autocomplete="off" class="layui-input" value="<?php echo $cate_now['cate_name']; ?>">
                    </div>
                </div>

                <!--添加一个排序字段-->

                <div class="layui-form-item">
                    <label for="cate_order" class="layui-form-label">
                        <span class="x-red">*</span>排序
                    </label>
                    <div class="layui-input-inline">
                        <input type="text" id="cate_order" name="cate_order" required="" lay-verify="required"
                               autocomplete="off" class="layui-input" value="<?php echo $cate_now['cate_order']; ?>">
                    </div>
                </div>





                <div class="layui-form-item">
                    <label class="layui-form-label">所属分类</label>
                    <div class="layui-input-inline" >
                        <select name="uid">
                            <option value="0">顶级分类</option>

                            <?php if(is_array($cate_level) || $cate_level instanceof \think\Collection || $cate_level instanceof \think\Paginator): $i = 0; $__LIST__ = $cate_level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                            <!--将当前分类置为选中状态-->
                            <?php if($cate_now['pid'] == $vo['pid']): ?>
                            <option value="<?php echo $vo['pid']; ?>" selected><?php echo $vo['cate_name']; ?></option>
                            <?php else: ?>
                            <option value="<?php echo $vo['pid']; ?>" ><?php echo $vo['cate_name']; ?></option>
                            <?php endif; endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>
                </div>
                
                <div class="layui-form-item">
                    <label for="" class="layui-form-label">
                    </label>
                    <button  class="layui-btn" lay-filter="save" lay-submit="" >
                        保存
                    </button>
                </div>
            </form>
        </div>
        <script src="__STATIC__/admin/lib/layui/layui.js" charset="utf-8"></script>
<script src="__STATIC__/admin/js/x-admin.js"></script>
<script src="__STATIC__/admin/js/jquery.min.js"></script>
<script src="__STATIC__/admin/js/x-layui.js"></script>
<!--引入boostrap-->
<link rel="stylesheet" type="text/css" href="__STATIC__/admin/lib/bootstrap/css/bootstrap.css" />
<script type="text/javascript" src="__STATIC__/admin/lib/bootstrap/js/bootstrap.js"></script>
        <script>
            layui.use(['form','layer'], function(){
                $ = layui.jquery;
              var form = layui.form()
              ,layer = layui.layer;
            

              //监听提交
              form.on('submit(save)', function(data){
                console.log(data);
                //发异步，把数据提交给php

                  $.post("<?php echo url('update'); ?>", data.field, function (res) {
                      if (res.status == 1) {
                          layer.alert(res.message, {icon: 6},function () {
                              // 获得frame索引
                              var index = parent.layer.getFrameIndex(window.name);
                              //关闭当前frame
                              parent.layer.close(index);
                          });


                      }else {

                          layer.alert(res.message, {icon: 6},function () {
                              // 获得frame索引
                              var index = parent.layer.getFrameIndex(window.name);
                              //关闭当前frame
                              parent.layer.close(index);
                          });

                      }
                  });

                return false;
              });
              
              
            });
        </script>
        <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
    </body>

</html>