<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Search extends Base
{
	//搜索引擎
	public function index(){
		//获得搜索引擎一级表数据
    	$res = Db::table('category')->select();
    	//获取信息流营销一级表数据
    	$resu = Db::table('categoryxx')->select();
    	//微信朋友圈一级表
        $weixin = Db::table('categorywx')->select();
    	//获取二级表banner数据
    	$data = Db::table('cate')->where('pid',input('pid'))->find();
        //获取页面主体图片数据
        $zhuti = Db::table('cate_zhuti')->where('pid',input('pid'))->find();
    	//获取案例轮播图数据
    	$lunbo = Db::table('cate_img')->where('pid',input('pid'))->select();
		//网站头部
		$header = Db::table('header')->select();
		//社交广告
        $shejiao  = Db::table('shejiao')->select();
        //信息流广告
        $liuguang = Db::table('liuguang')->select();
        //友情链接
        $link = Db::table('link')->select();

		$this->assign('header',$header);
		$this->assign('res',$res);
		$this->assign('resu',$resu);
		$this->assign('weixin',$weixin);
		$this->assign('data',$data);
        $this->assign('zhuti',$zhuti);
		$this->assign('lunbo',$lunbo);
		$this->assign('shejiao',$shejiao);
        $this->assign('liuguang',$liuguang);
        $this->assign('link',$link);

		return $this->fetch('search');
	}
}