<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Index extends Base
{	
	//首页
    public function index()
    {	
    	
    	//轮播Banner图
    	$banner = Db::table('banner_sy')->select();
    	//产品服务
    	$product = Db::table('product_fuwu')->find();
    	//博盛天达优势服务
    	$bosheng = Db::table('bosheng_fuwu')->find();
    	//合作伙伴
    	$partners = Db::table('partners')->select();
    	//获得搜索引擎一级表数据
    	$res = Db::table('category')->select();
    	//获得信息流营销一级表数据
    	$resu = Db::table('categoryxx')->select();
        //微信朋友圈一级表
        $weixin = Db::table('categorywx')->select();
		//网站头部
		$header = Db::table('header')->select();
        //社交广告
        $shejiao  = Db::table('shejiao')->select();
        //信息流广告
        $liuguang = Db::table('liuguang')->select();
        //友情链接
        $link = Db::table('link')->select();
		
    	$this->assign('banner',$banner);
    	$this->assign('product',$product);
    	$this->assign('bosheng',$bosheng);
    	$this->assign('partners',$partners);
    	$this->assign('res',$res);
    	$this->assign('resu',$resu);
        $this->assign('weixin',$weixin);
    	$this->assign('header',$header);
        $this->assign('shejiao',$shejiao);
        $this->assign('liuguang',$liuguang);
        $this->assign('link',$link);
    	
        return $this->fetch();
    }
}
