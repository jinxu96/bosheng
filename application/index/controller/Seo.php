<?php
namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Seo extends Base
{
	//央视广告
	public function index(){
		//网站头部
		$header = Db::table('header')->select();
		//获得搜索引擎一级表数据
    	$res = Db::table('category')->select();
    	//获取信息流营销一级表数据
    	$resu = Db::table('categoryxx')->select();
    	//微信朋友圈一级表
        $weixin = Db::table('categorywx')->select();
        //获取页面主体图片数据
        $zhuti = Db::table('seo_zhuti')->find();
        //banner图
        $banner = Db::table('banner')->select();
        //轮播图
        $lunbo  = Db::table('seo')->select();
        //社交广告
        $shejiao  = Db::table('shejiao')->select();
        //信息流广告
        $liuguang = Db::table('liuguang')->select();
        //友情链接
        $link = Db::table('link')->select();
		
		$this->assign('header',$header);
		$this->assign('res',$res);
		$this->assign('resu',$resu);
		$this->assign('weixin',$weixin);
        $this->assign('zhuti',$zhuti);
		$this->assign('banner',$banner);
		$this->assign('lunbo',$lunbo);
		$this->assign('shejiao',$shejiao);
        $this->assign('liuguang',$liuguang);
        $this->assign('link',$link);

		return $this->fetch('seo');
	}
	
}