<?php

namespace app\index\controller;

use app\admin\common\Base;
use think\Request;
use think\Db;

class Contact extends Base
{
	//联系我们
    public function index(){
        //网站头部
        $header = Db::table('header')->select();
        //获得搜索引擎一级表数据
        $res = Db::table('category')->select();
        //获取信息流营销一级表数据
        $resu = Db::table('categoryxx')->select();
        //微信朋友圈一级表
        $weixin = Db::table('categorywx')->select();
        //社交广告
        $shejiao  = Db::table('shejiao')->select();
        //信息流广告
        $liuguang = Db::table('liuguang')->select();
        //友情链接
        $link = Db::table('link')->select();
        
        $this->assign('header',$header);
        $this->assign('res',$res);
        $this->assign('resu',$resu);
        $this->assign('weixin',$weixin);
        $this->assign('shejiao',$shejiao);
        $this->assign('liuguang',$liuguang);
        $this->assign('link',$link);

        return $this->fetch('contact');
    }
    public function add(){
        //判断页面是否提交
        // print_r($_POST);exit();
        if(request()->isPost()){
            //接收传递过来的值
            //input都是封装好了，相当于POST['xxx'];
            $data = [
                'name' => input('name'),
                'phone' => input('phone'),
                'nc' => input('nc'),
                'content' => input('content'),
                'time' => time()
            ];
            // dump($data);exit;
            if(Db::table('message')->insert($data)){  //添加数据
                return "<script>alert('留言成功');window.location.href='index';</script>";
            }else{
                return "<script>window.location.href='index';</script>";
            }
            return;
            
        }
        return $this->fetch('contact');
    }
}