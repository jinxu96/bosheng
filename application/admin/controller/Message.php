<?php

namespace app\admin\controller;
use app\admin\common\Base;
use think\Request;
use think\Db;
use app\admin\model\Message as MessageModel;

class Message extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //分页数据
        $message = Db::table('message')->paginate(5);
        // dump($message);exit;
        $count = Db::table('message')->Count();
        //分配数据
        $this->assign('message',$message);
        $this->assign('count',$count);
        //渲染
        return $this->fetch('message_list');

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
        MessageModel::destroy($id);

    }
}
