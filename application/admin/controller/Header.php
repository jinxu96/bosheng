<?php

namespace app\admin\controller;

use app\admin\common\Base;
use think\Db;
use think\Request;

use app\admin\model\Header as HeaderModel;
class Header extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //分页数据
        $gongc = Db::table('header')->paginate(4);
        // dump($gongc);exit;
        $count = Db::table('header')->Count();
        //分配数据
        $this->assign('gongc',$gongc);
        $this->assign('count',$count);
        //渲染
        return $this->fetch('header_list');
    }

    /**
     * 显示创建资源表单页
     *
     * @return \think\Response
     */
    public function create()
    {
        //
        return $this->view->fetch('header_add');

    }

    /**
     * 保存新建的资源
     * @return \think\Response
     */
    public function save()
    {
        //判断一下提交类型
        if ($this->request->isPost()) {

            //1.获取一下提交的数据,包括上传文件
            $data = $this->request->param(true);

            $res = HeaderModel::create($data);

            //6判断新增是否成功
            if (is_null($res)){
                $this->error('新增失败');
            }

            $this->success('新增成功~~');

        }else {
            $this -> error('请求类型错误~~');
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //1.查询要编辑的记录
        $data = HeaderModel::get($id);

        //2.将查询结果赋值给模板
        $this -> view -> assign('data', $data);

        //3.渲染模板
        return $this->view->fetch('header_edit');
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update()
    {
        //1.获取所有提交过来的数据，包括文件
       $data = $this ->request -> param(true);

        //4.执行更新操作
        $res = HeaderModel::update([
            'title_name' => $data['title_name'],
            'keywords' => $data['keywords'],
            'description' => $data['description'],
        ],['id'=> $data['id']]);

        //5.检测更新
        if (is_null($res)) {
            $this -> error('更新失败~~');
        }

        //6.更新成功
        $this->success('更新成功~~');



    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
        HeaderModel::destroy($id);

    }
}
